import stringdef addToStack(val:str):
    global mem
    global prevVar
    global valueInStack
    #Checking for ascii values
    if val in list(string.ascii_letters):
    #Initially the previous value is initialized to 0 if it is already present it will be stored in the stack
        if prevVar:
            if(valueInStack):
                print('ST $',mem)
                mem+=1
            print("L",prevVar)
            valueInStack = True
        prevVar = val
    #Checking whether the value is UnaryOperator
    if val == '@':
        if prevVar:
            if(valueInStack):
                print('ST $',mem)
                mem+=1
            print("L",prevVar)
            prevVar = 0
        print('N')
    #Checking if the value is BinaryOperator
    if val in ['+','-','*','/']:

        #Performs Addition operation
        if val == '+':
            if(prevVar):
                print('A {}'.format(prevVar))
                prevVar = 0
            else:
                mem-=1
                print('A ${}'.format(mem))
        #Performs Subtraction operation
        if val == '-':
            if(prevVar):
                print('S',prevVar)
                prevVar = 0
            else:
                mem-=1
                print('N')
                print('A $',mem)
        #Performs Multiplication Operation
        if val == '*':
            if(prevVar):
                print('M',prevVar)
                prevVar = 0
            else:
                mem-=1
                print('M $',mem)
        #Performs Division Operation
        if val == '/':
            if(prevVar):
                print('D',prevVar)
                prevVar = 0
            else:
                print('ST $',mem)
                mem+=1
                print('L$',mem-2)
                mem-=1
                print('D $',mem)
                mem-=1
inp=input("Enter desired Postfix expression: ")
#first = True
#if(not first): 
    #print()
#else:
#    first=False
mem=1
prevVar=0
valueInStack=False
for i in inp:
    addToStack(i)
addToStack(0)
