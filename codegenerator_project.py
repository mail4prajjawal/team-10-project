import string
def addToStack(val:str):
    global mem
    global prevVar
    global valueInStack
    if val in list(string.ascii_letters):
        if prevVar:
            if(valueInStack):
                print('ST $',mem)
                mem+=1
            print("L ",prevVar)
            valueInStack = True
        prevVar = val
    if val == '@':
        if prevVar:
            if(valueInStack):
                print('ST $',mem)
                mem+=1
            print("L ",prevVar)
            prevVar = 0
        print('N')
    if val in ['+','-','*','/']:
        if val == '+':
            if(prevVar):
                print('A  {}'.format(prevVar))
                prevVar = 0
            else:
                mem-=1
                print('A ${}'.format(mem))
        if val == '-':
            if(prevVar):
                print('S ',prevVar)
                prevVar = 0
            #negation 
            else:
                mem-=1
                print('N')
                print('A $',mem)
        if val == '*':
            if(prevVar):
                print('M ',prevVar)
                prevVar = 0
            else:
                mem-=1
                print('M $',mem)
        if val == '/':
            if(prevVar):
                print('D ',prevVar)
                prevVar = 0
            else:
                print('ST $',mem)
                mem+=1
                print('L $',mem-2)
                mem-=1
                print('D $',mem)
                mem-=1

print("Please enter the expression: ")
inp=input()
#first = True
#if(not first): 
   # print()
#else:
    #first=False
mem=1
prevVar=0
valueInStack=False
for i in inp:
    addToStack(i)
addToStack(0)